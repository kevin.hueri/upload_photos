<?php
var_dump($_FILES);
if (isset($_FILES['my_file'])){
    if ($_FILES['my_file']['type']=="image/jpeg"){
        if ($_FILES['my_file']['size']<1000000) {
            [$width, $height] = getimagesize($_FILES['my_file']['tmp_name']);
            if ($width>800 && $height>800) {
                uploadImage($_FILES['my_file']['tmp_name'], $width, $height);
            } else {
                echo "image trop petite";
            }
        } else {
            echo "Fichier trop gros";
        }
    } else {
        echo "
        Typde de fichier invalide";
    }
}
function uploadImage($imagePath, $imgwidth, $imgheight){
    $fileName =  rand(10000, 99999) . ".jpeg";

    move_uploaded_file($imagePath, "uploads/" .$fileName);
    $srcImage = imagecreatefromjpeg("uploads/".$fileName);
    $ratio = $imgwidth/$imgheight;
    if ($ratio > 1){
        $miniWidth = 150;
        $miniHeight = 150/$ratio;
    } else {
        $miniWidth = 150*$ratio;
        $miniHeight = 150;
    }
    $miniature = imagecreatetruecolor($miniWidth,$miniHeight);
    imagecopyresampled($miniature, $srcImage, 0, 0, 0, 0, 150, 150, $imgwidth, $imgheight);
    imagejpeg($miniature, "miniatures/".$fileName);
    $watermark = imagecreatefrompng("watermark.png");
    $watermarkSize = getimagesize("watermark.png");
    imagecopyresampled($srcImage, $watermark, 0, 0, 0, 0, $imgwidth, $imgheight, $watermarkSize[0], $watermarkSize[1]);
    imagejpeg($srcImage, "uploads/".$fileName);

}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form enctype="multipart/form-data" method="post">
    <input type="file" name="my_file">
    <input type="submit">
</form>

</body>
</html>