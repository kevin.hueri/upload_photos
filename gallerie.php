<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="conteneur">
    <h1>GALERIE</h1>
    <?php
    $files = glob("images/*");
    //var_dump($files);
    foreach ($files AS $file){
        echo "<img class='gallerie-img' src='$file'/>";
    }
    ?>
    <div style="text-align: right;"><a href="index.php">Envoyer un fichier</a></div>
</div>
</body>
</html>